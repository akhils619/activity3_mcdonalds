import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestMcDonalds {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		// 1. Configure Selenium to talk to Chrome
		System.setProperty("webdriver.chrome.driver", "/Users/akhilsraj/Desktop/chromedriver");
		driver = new ChromeDriver();
		
		// 2. Enter the website you want to go to
		String baseUrl = "https://www.mcdonalds.com/";
		
		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);

	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(10000); // Pause for 1 second before closing the browser
		driver.close();
	}

	//TC1 Implementation
	@Test
	public void testSubscriptionTitle() {
		//Select the title for the subscribe section 
		WebElement subscribeTitle = driver.findElement(By.cssSelector("h2.click-before-outline"));
		//Checking if the given title mateches the title in the Webpage
		assertEquals("Subscribe to My McD’s®", subscribeTitle.getText());
	}

	//TC2 Implementation
	@Test
	public void testSignUpPositive() throws InterruptedException {
		
		//Select the firstname for the subscribe section 
		WebElement firstName = driver.findElement(By.id("firstname2"));
		firstName.sendKeys("Akhil");
		
		//Select the email for the subscribe section 
		WebElement email = driver.findElement(By.id("email2"));
		email.sendKeys("akhils619@gmail.com");

		//Select the email for the subscribe section 
		WebElement postalCode = driver.findElement(By.id("postalcode2"));
		postalCode.sendKeys("L5V2C9");

		//Select the subscribe button for the subscribe section 
		WebElement subscribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));
		subscribeButton.click();

	}

	//TC3 Implementation
	@Test
	public void testSignUpNegative() {

		//Select the firstname for the subscribe section 
		WebElement firstName = driver.findElement(By.id("firstname2"));
		//Sending blank string
		firstName.sendKeys("");
		
		//Select the email for the subscribe section 
		WebElement email = driver.findElement(By.id("email2"));
		//Sending blank string
		email.sendKeys("");
		
		//Select the email for the subscribe section 
		WebElement postalCode = driver.findElement(By.id("postalcode2"));
		//Sending blank string
		postalCode.sendKeys("");
		
		//Select the subscribe button for the subscribe section 
		WebElement subscribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));
		subscribeButton.click();
		
	}
}
